---
title: Besoin d'aide ?
menu:
  main:
    weight: 4
toc: true
---

Nous regroupons dans cette page quelques questions fréquemment posées et leurs réponses dans les cas usuels. Si après avoir lu cette page et essayé les solutions proposées le problème persiste, envoie-nous un mail à notre adresse de contact (contact at crans point org). Pour faciliter le traitement, essaye de nous donner un maximum d'informations. Tu peux t'inspirer du canevas ci-dessous.

> Bonjour, je suis (prénom/nom) mon pseudo Crans est (pseudo). J'essaye de (action) en (donner si possible des détails), mais je n'y arrive pas. J'obtiens (message d'erreur/réaction non voulue) à la place. Pouvez-vous m'aider s'il-vous-plaît ?

## J'ai perdu mon accès à mon compte Crans

Tu peux réinitialiser ton mot de passe en cliquant [ici](https://intranet.crans.org/users/reset_password/). En **aucun cas** ne recrée de compte, ça ne sert strictement à rien.

## Quels services offrez-vous ?

En adhérant, tu peux accéder à l'ensemble des services du Crans listés sur <https://services.crans.org/>.

## Quels services offrez-vous aux ancien·ne·s adhérent·e·s ?

Les ancien·ne·s adhérent·e·s peuvent encore utiliser l'ensemble des services à vie, hormis le service d'accès à Internet qui requiert des paiements réguliers.

## Je n'arrive pas à me connecter à photos.crans.org, note.crans.org...

Ces services ne sont pas gérés directement par le Crans mais par des adhérent·e·s. En cas de souci, contactez respectivement les administrateurs de ces sites.

## J'ai besoin d'aide et on met longtemps à me répondre

Les membres de notre association sont bénévoles et consacrent déjà beaucoup de temps pour l'association en plus de leurs propres obligations étudiantes ou professionnelles. Iels font déjà ce qu'iels peuvent et il est important de respecter leur vie privée.
