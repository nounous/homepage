---
title: Qui sommes-nous ?
menu:
  main:
    weight: 1
---

**Le Crans est l'association réseau de l'ENS Paris-Saclay**,
nous fournissons des services numériques aux usagé·ère·s de l'ENS Paris-Saclay,
ainsi qu'une couverture internet filaire aux associations
et clubs de l'ENS Paris-Saclay. 

Aujourd'hui l'association fournit des services tels
qu'[une boîte mail à vie](https://intranet.crans.org/users/mon_profil/#email)
ou [un hébergement de pages personnelles](https://perso.crans.org/).

**Le Crans a également un but pédagogique** ;
nous organisons par exemple
[des séminaires techniques hebdomadaires](https://wiki.crans.org/CransTechnique/CransApprentis/SeminairesTechniques)
sur des thèmes généraux ou spécifiques au Crans,
comme la gestion d'un réseau et les protocoles et services associés.

L'association a aussi pour but de promouvoir l'utilisation
des logiciels libres auprès de ses adhérents comme auprès du grand public :
organisation d'événements comme [les install party](https://install-party.crans.org/)
annuelles, qui sont également l'occasion de conférences,
mais aussi par exemple hébergement d'un miroir de [VideoLAN](https://www.videolan.org/), de [Debian](https://www.debian.org/) et d'[Ubuntu](https://ubuntu.com/).
L'association est également adhérente de l'[APRIL](https://april.org/).
Le Crans est membre de [FedeRez](https://www.federez.net/),
fédération d'associations étudiantes visant à
coordonner les échanges entre associations d'informatique.

<article class="message is-info">
  <div class="message-header is-size-5">
    <p>Je veux aider !</p>
  </div>
  <div class="message-body is-size-5">
    Si tu as envie de découvrir l'envers du décor, que ce soit la gestion
    administrative d'une association ou la gestion technique
    de l'infrastructure, tu es le/la bienvenu·e. Il n'y a pas de connaissance
    particulière à avoir pour commencer, mais si tu nous rejoins, tu auras
    l'occasion d'en acquérir de nombreuses. Pour commencer, le plus simple est
    de prendre contact avec nous, par exemple en nous croisant à la Kfet.
    À bientôt !
  </div>
</article>
