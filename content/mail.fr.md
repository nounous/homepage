---
title: Configurer son email
#menu:
#  main:
#    weight: 4
toc: true
---

**Le Crans fournit à tout membre ayant adhéré une fois une adresse email `ton_pseudo@crans.org` valable à vie.**
Par défaut cette adresse est désactivée, mais vous pouvez aller sur votre profil sur [l'intranet](https://intranet.crans.org/users/mon_profil/) pour activer soit une redirection vers votre adresse principale, ou activer un compte hébergé sur nos serveurs.

Il est également possible de définir des alias qui redirigeront vers cette adresse de courriel.

## Sous Thunderbird

Thunderbird est un client de courriel libre, issu du même projet que Firefox.
Le Crans fournit une autoconfiguration pour ce client. Vous pouvez donc aller dans « *Menu > Nouveau > Compte courrier existant...* » et directement mettre votre email et mot de passe Crans.

## Autres clients

<article class="message is-info">
  <div class="message-header is-size-5">
    <p>Attention ! Ne vous connectez pas avec votre email en identifiant.</p>
  </div>
  <div class="message-body is-size-5">
    Certains clients de courriel considèrent que l'identifiant de connexion au serveur de courrier est l'adresse de courriel (en @crans.org).
    Néanmoins dans notre cas votre identifiant doit être votre identifiant Crans.
  </div>
</article>

Le serveur de courriel entrant est `imap.crans.org` en connexion sécurisée TLS/SSL sur le port 993.

Le serveur de courriel sortant est `smtp.crans.org` en connexion sécurisée TLS/SSL sur le port 465.

Dans les deux cas **il est nécessaire de s'identifier** avec son identifiant et mot de passe Crans.
