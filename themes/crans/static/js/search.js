/*
Some JavaScript for search, Crans 2020
*/

new autoComplete({
    data: {
        src: async () => {
            const source = await fetch(indexUrl);
            const data = await source.json();
            return data;
        },
        key: ["title", "content"],
        cache: true
    },
    highlight: true,
    resultsList: {
        render: true
    },
    resultItem: {
        content: (data, source) => {
            found = data.match.match(/([^\s]*.{0,32}<span.*>.{0,32}[^\s]*)/);
            if (found) {
                source.innerHTML = "<b>" + data.value.title + "</b> : " + found[1];
            } else {
                // erdnaxe missed his regex...
                console.log(data.match);
            }
        }
    },
    onSelection: feedback => {
        // Go to page
        window.location.href = feedback.selection.value.uri;
    }
});
